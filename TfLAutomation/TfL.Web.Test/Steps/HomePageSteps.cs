﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using TfL.Web.Test.Interfaces;

namespace TfL.Web.Test.Steps
{
    [Binding]
    public class HomePageSteps
    {
        public IHomePage HomePage;
        public HomePageSteps(IHomePage homePage)
        {
            HomePage = homePage;
        }

        [Given(@"I am on TFL HomePage")]
        public void GivenIamOnTflHomePage()
        {
            HomePage.NavigateToSpecifiedURLSection("");
            Assert.IsTrue(HomePage.GetTitleFromPageSource().ToString().Trim().ToLower().Contains("transport for london"));
        }

    }
}