﻿using BoDi;
using System.Diagnostics;
using System.Configuration;
using TechTalk.SpecFlow;
using TfL.Web.Test.Interfaces;
using TfL.Web.Test.Pages;
using TfL.Web.Test.WebDriver;

namespace TfL.Web.Test.Steps
{
    [Binding]
    public class Initializer
    {
        private readonly IObjectContainer _objectContainer;       
        public IHomePage HomePage;        
        public Initializer(IObjectContainer objectContainer)
        {
            _objectContainer = objectContainer;
        }

        [BeforeScenario]
        public void Initialize()
        {                        
            if (GetBrowser() != "")
            {
                if (Driver.Instance == null)
                    Driver.Initialize();
                else
                {
                    Driver.Instance.Manage().Cookies.DeleteAllCookies();
                }
            }

            if (Driver.Instance != null)
            {
                HomePage = new HomePage(Driver.Instance);
                _objectContainer.RegisterInstanceAs(HomePage);               
            }
        }

        public static string ReadSetting(string key)
        {
            string result = null;
            try
            {
                var appSettings = ConfigurationManager.AppSettings;
                result = appSettings[key] ?? "Not Found";
                return result;
            }
            catch (ConfigurationException)
            {
                Debug.WriteLine("Error reading app setting");
            }
            return result;
        }

        public static string GetBaseWebUrl()
        {
            return ReadSetting("TestWebEnvironment");
        }

        public static string GetBrowser()
        {
            return ReadSetting("Browser");
        }

        [AfterTestRun]
        public static void CloseDriver()
        {
            Driver.Close();            
        }        
    }
}
