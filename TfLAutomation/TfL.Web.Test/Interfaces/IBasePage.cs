﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TfL.Web.Test.Interfaces
{
    public interface IBasePage
    {
        string GetPageTitle();
        void ClickOnLink(string linkText);
        string GetTitleFromPageSource();
        void NavigateToSpecifiedURLSection(string sectionName);
             
    }
}