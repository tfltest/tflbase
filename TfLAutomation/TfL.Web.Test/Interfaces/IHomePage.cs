﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TfL.Web.Test.Interfaces
{
    public interface IHomePage : IBasePage
    {
        void ClickOnStatusLink(string status);
        void SearchByBusOrStopName(string busNumber);
    }
}