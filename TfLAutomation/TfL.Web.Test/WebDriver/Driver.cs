﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TfL.Web.Test.Steps;

namespace TfL.Web.Test.WebDriver
{
    public class Driver
    {
        public static IWebDriver Instance { get; set; }
        public static string BaseAddress => Initializer.GetBaseWebUrl();
        public static void Initialize()
        {
            var browser = Initializer.GetBrowser();
            switch (browser)
            {
                case "chrome":
                    ChromeOptions chOptions = new ChromeOptions();
                    var service = ChromeDriverService.CreateDefaultService(@GetDriverPath());
                    Instance = new ChromeDriver(service, chOptions, TimeSpan.FromSeconds(120));
                    Instance.Manage().Window.Maximize();
                    break;
            }
            Instance.Navigate().GoToUrl(BaseAddress);
            Instance.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);
        }

        public static void GoToBaseUrl()
        {
            Instance.Navigate().GoToUrl(BaseAddress);
            Instance.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);

        }

        public static void Close()
        {
            Instance.Close();
        }

        private static string GetProxy()
        {
            var proxy = Environment.GetEnvironmentVariable("HTTPS_PROXY");
            return proxy;
        }

        private static string GetDriverPath()
        {
            var requiredPath = Path.GetDirectoryName(Path.GetDirectoryName(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase)));
            requiredPath = requiredPath?.Replace("file:\\", "") + "\\drivers";
            Trace.WriteLine(requiredPath);
            return requiredPath;
        }
    }
}