﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using TfL.Web.Test.Interfaces;
using TfL.Web.Test.Steps;

namespace TfL.Web.Test.Pages
{
    public abstract class BasePage : IBasePage
    {
        protected IWebDriver Driver;
        public const int DefaultWaitInSeconds = 15;
        public const int DefaultSleepInMilliseconds = 250;

        // [FindsBy(How = How.CssSelector, Using = "h1")]
        // protected IWebElement PageTitle { get; set; }
        protected IWebElement PageTitle => Driver.FindElement(By.CssSelector("h1"));
        protected BasePage(IWebDriver driver)
        {
            Driver = driver;       
        }

        public string GetPageTitle()
        {
            return PageTitle.Text;
        }

        public void ClickOnLink(string linkText)
        {
            Driver.FindElement(By.PartialLinkText(linkText)).Click();

        }

        public void ClickOnFullLink(string linkText)
        {
            IWebElement element = Driver.FindElement(By.LinkText(linkText));
            element.Click();
        }

        public string GetTitleFromPageSource()
        {
            return Driver.Title;
        }

        public void ClickElement(IWebElement element)
        {
            element.Click();
        }

        public void EnterInputValue(IWebElement inputElement, string textValue)
        {
            inputElement.Clear();
            inputElement.SendKeys(textValue);
        }

        public string GetElementText(IWebElement inputElement)
        {             
            var javaScriptExecutor = (IJavaScriptExecutor)Driver;
            Thread.Sleep(1000);
            string x = (string)(javaScriptExecutor.ExecuteScript("return arguments[0].innerText", inputElement));
            Debug.WriteLine(x);
            return x;
        }

       

        public void NavigateToSpecifiedURLSection(string sectionName)
        {          
            var baseUrl = Initializer.GetBaseWebUrl();
            Console.WriteLine(baseUrl + sectionName);
            Driver.Navigate().GoToUrl(baseUrl + sectionName);
            Thread.Sleep(2000);
        
        }
        
        public string GetText(IWebElement inputElement)
        {
            return inputElement.Text;
        }


        protected void SelectValueFromAutoSuggestion(string fromStation)
        {
            IList<IWebElement> stops = Driver.FindElements(By.ClassName("stop-name"));
            foreach (IWebElement el in stops)
            {
                String text = el.Text;
                if (text.Contains(fromStation))
                {
                    try
                    {
                        Thread.Sleep(3000);
                    }
                    catch (Exception e)
                    {
                        Trace.WriteLine(e.Message);
                    }
                    ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].click();", el);
                    break;
                }
            }
        }

        protected void SelectFromValueListDropDown(IWebElement element, string text)
        {
            SelectElement select = new SelectElement(element);
            select.SelectByText(text);
        }

        public void SelectFromValueListDropDownUsingIndex(IWebElement element, int index)
        {
            SelectElement select = new SelectElement(element);
            select.SelectByIndex(index);
        }
    }
}
