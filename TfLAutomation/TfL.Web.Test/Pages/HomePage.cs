﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using TfL.Web.Test.Interfaces;

namespace TfL.Web.Test.Pages
{
    public class HomePage : BasePage, IHomePage
    {
        public HomePage(IWebDriver driver) : base(driver)
        {
            
        }

        [FindsBy(How = How.XPath, Using = ".//*[@id='main-hero']/div/div[1]/div[2]/a[2]")]
        protected IWebElement MapsLink { get; set; }     

        public void ClickOnStatusLink(string status)
        {

        }

        public void SearchByBusOrStopName(string busNumber)
        {
            ClickElement(Driver.FindElement(By.CssSelector(".always-visible.accordion-heading.tracked")));
            EnterInputValue(Driver.FindElement(By.Id("Input")), busNumber);
            SelectValueFromAutoSuggestion(busNumber);
        }       
    }
}